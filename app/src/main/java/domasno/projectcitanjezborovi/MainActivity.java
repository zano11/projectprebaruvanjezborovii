package domasno.projectcitanjezborovi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.InputStream;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onLookUp(View view) {

        // пристап до пребарувачот
        EditText zbor = (EditText)  findViewById(R.id.zbor);
        String zborot = zbor.getText().toString();

        // prebaruvanje vo txt file

        String definition = findDefention(zborot);

        TextView prikazot = (TextView) findViewById(R.id.prikaz);

        if (definition != null)

            prikazot.setText(definition);
        else
            prikazot.setText("Зборот не е пронајден");


    }

    private String findDefention(String zborot) {

        //pristap do txt

        InputStream input = getResources().openRawResource(R.raw.zborovinamakjazik);


        Scanner scan = new Scanner(input);

        while(scan.hasNext()){
            String line = scan.nextLine();// go cita cela redica

            String[] deluvac = line.split("=");

            if(deluvac[0].equalsIgnoreCase(zborot.trim())){
                return deluvac[1];
            }

        }
return null;


    }
}
